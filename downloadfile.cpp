#include "downloadfile.h"
#include <QFileInfo>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QString>
#include <QTimer>
#include <stdio.h>


DownloadFile::DownloadFile(QObject *parent)
    :QObject(parent), notify(QString::null), file_size(0)
{

}


QString DownloadFile::saveFileName(const QUrl &url)
{
    QString path = url.path();

    //để đây và không nói thì thêm :P
    //QFileInfo fi("/tmp/archive.tar.gz");
    //QString name = fi.fileName();                // name = "archive.tar.gz"
    QString basename = QFileInfo(path).fileName();

    if(basename.isEmpty()){
        basename = "Dowloadfile";
    }

    if(QFile::exists(basename)){
        int index = 0;
        basename += '.';

        //Trả về một chuỗi tương đương với số i theo cơ số(mặc định cơ số 10) đã chỉ định.
        //QString::number(index)

        while(QFile::exists(basename + QString::number(index))){
            index++;
        }

        basename += QString::number(index);
    }

    return basename;

}

void DownloadFile::startNextdownload()
{
    //test chuong trinh co chia thanh luong hay khong
   // for(int i = 0; i < 100000; i++)
    //    qDebug() << i << "hello linh van";

    QUrl url = this->url;

    QString filename = saveFileName(url);

    //output la QFile
    //vi tri luu file
    output.setFileName("E:/Thuc Tap He 2017/Qt basic/data/" + filename);

    if(!output.open(QIODevice::WriteOnly)){//khong ton tai file
        fprintf(stderr, "Problem opening save file '%s' for download '%s': %s\n",
                qPrintable(filename), url.toEncoded().constData(),
                qPrintable(output.errorString()));

        startNextdownload();
        return;
    }


    //tien hanh dowload file tu tren mang
    //2 câu lệnh ở dưới tương ứng với  manager->get(QNetworkRequest(QUrl(url)));
    QNetworkRequest request(url);
    currentDownload = manager.get(request);
    qDebug() << "Url can dowload la: " << url;

    connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)),
            SLOT(downloadProgress(qint64,qint64)));

    connect(currentDownload, SIGNAL(finished()),
               SLOT(downloadFinished()));

    //void QIODevice::readyRead() là  phát ra mỗi lần dữ liệu
    //mới có sẵn để đọc từ kênh đọc hiện tại của thiết bị
    connect(currentDownload, SIGNAL(readyRead()),
            SLOT(downloadReadyRead()));

    printf("Downloading %s...\n", url.toEncoded().constData());
        // prepare the output
    downloadTime.start();//tính thời gian dowload file

}

void DownloadFile::downloadReadyRead()
{
    /*
      tương đương với câu lệnh trên
      QFile *file = new QFile("E:/Thuc Tap He 2017/Qt basic/data/" + yoururl.toString());
      if(file->open(QFile::Append))
      {
            file->write(reply->readAll());
            file->flush();
            file->close();
       }
       delete file;
    */

    output.write(currentDownload->readAll());//doc file
}

//signal của QNetworkReply, số byte nhận được khi tải trang theo từng tiến trình tải
void DownloadFile::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    double speed = bytesReceived*1000.0 / downloadTime.elapsed();
   // qDebug() << "Tong so byte " << bytesReceived << "    " <<  bytesTotal << "   " << downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
         unit = "bytes/sec";
    } else if (speed < 1024*1024) {
         speed /= 1024;
         unit = "kB/s";
    } else {
         speed /= 1024*1024;
         unit = "MB/s";
    }

    if(bytesTotal > this->file_size){
        //lay kich thuoc file
        this->file_size = bytesTotal;
        //toc do dowload trung binh
        this->speed_average =  QString::number(speed) + " " + unit;
        qDebug() << "Toc do dowload: " << this->speed_average;
    }
    //thoi gian download tong cong
    this->time_down_load = downloadTime.elapsed();

    emit noteTime();


}

QString DownloadFile::Notify()
{
    return this->notify;
}

void DownloadFile::downloadFinished()
{
    output.close();

    if (currentDownload->error()) {
        // download failed
        fprintf(stderr, "Failed: %s\n", qPrintable(currentDownload->errorString()));
        this->notify = "Error!!!" + currentDownload->errorString() + ". Try again.";
    } else {
        printf("Succeeded.\n");
        this->notify = "Succeeded!!! Please go: E:/Thuc Tap He 2017/Qt basic/data/... to check file.";
    }

    currentDownload->deleteLater();

    emit finished();//ban tin hieu ket thuc xong qua trinh download file

    return;
}
