#ifndef CONNECT_H
#define CONNECT_H

#include <QObject>
#include <QUrl>
#include <QDebug>
#include <QString>
#include <QTimer>
#include "downloadfile.h"

class Connect : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString urlNetwork READ urlNetwork WRITE setUrlNetwork NOTIFY urlNetworkChanged)


public:
    explicit Connect(QObject *parent = nullptr);
    QString urlNetwork();
    void setUrlNetwork(const QString &urlNetwork);


signals:
    void urlNetworkChanged();
    void takeUrlfromQml(QString url_network);


public slots:
    void ReplyfromCtoQml(QString url);
    void setUrlNetwork1();
    void time_now();

private:
    QString m_urlNetwork;
    QString m_notify;
    DownloadFile *Download;
};

#endif // CONNECT_H
