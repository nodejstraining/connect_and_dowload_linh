
import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import io.qt.examples.connect 1.0

Window {
    visible: true
    width: 640
    height: 640
    title: qsTr("Enter url")

    Connect {
       id: connect
    }

    Row{

        Rectangle{
            height: 40
            width: 150

            Rectangle{
                x: 76
                y: 40
                height: 30
                width: 60
                color: "orange"
                radius: 4

                Text {
                    anchors.centerIn: parent
                    text: "url"
                    font.pointSize: 10
                }
            }

            TextField {
                x: 140
                y: 40
                id: url
                text: "http://doc.qt.io/qt-4.8/qthread.html"
                 placeholderText: qsTr("Enter url...")

                style: TextFieldStyle {
                    textColor: "black"
                    background: Rectangle {
                        radius: 2
                        implicitWidth: 220
                        implicitHeight: 30
                        border.color: "#333"
                        border.width: 1
                    }

                    placeholderTextColor: "red"
                }
            }

        }

        Button{
            y: 80
            id: btnUpdate
            text: "Just click me"
            property int index: 0

            onClicked:{

                if(url.text == ""){
                    notify.text = "**Please fill url network to input box"
                }else{
                  //  notify.text = ""
                    connect.ReplyfromCtoQml(url.text)
                    for(index = 0; index <= 100; index++){
                        bar.value = index;
                    }
                    url.text = ""
                }
            }
        }

    }

    Column{
        y: 150
        id:c_p

        ProgressBar{
            value: 0
            id: bar
            maximumValue: 100
            minimumValue: 0
            x:100
        }
    }

    Text{
        y:200
        x:100
        id: notify
        wrapMode: Text.WordWrap
        text: "<b>**</b>" + connect.urlNetwork

    }

}
