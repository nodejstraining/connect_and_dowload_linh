#include "connect.h"
#include <QThread>

Connect::Connect(QObject *parent):
    QObject(parent), m_notify(QString::null)
{

}

QString Connect::urlNetwork()
{
    return m_notify;
}

void Connect::ReplyfromCtoQml(QString url)
{
    if(url == m_urlNetwork)
    {
        setUrlNetwork("Url này bị trùng lặp.");
        return;
    }else{
        QUrl yurl(url);

       //ham chay tien hanh dowload file
        Download = new DownloadFile();
        Download->url = yurl;
        QThread *thread1 = new QThread();
      //  Download->startNextdownload();

        //tien hanh chia luong cho giao dien va network(dowload du lieu)
        Download->moveToThread(thread1);

        connect( thread1, SIGNAL(started()), Download, SLOT(startNextdownload()));
        connect( Download, SIGNAL(finished()), thread1, SLOT(quit()));

        //automatically delete thread and task object when work is done:
        connect( thread1, SIGNAL(finished()), Download, SLOT(deleteLater()) );
        connect( thread1, SIGNAL(finished()), thread1, SLOT(deleteLater()) );

        thread1->start();
        setUrlNetwork("Dowloading from "+ url + "...");

        connect( Download, SIGNAL(finished()), this, SLOT(setUrlNetwork1()));
        //thong bao thoi gian
        connect( Download, SIGNAL(noteTime()), this, SLOT(time_now()));
    }
}

void Connect::setUrlNetwork(const QString &notify)
{
    m_notify = "";
    m_notify = notify;
    qDebug() <<  m_notify;
    emit urlNetworkChanged();
}

void Connect::setUrlNetwork1()
{
    m_notify = "";
    m_notify = Download->Notify();
    qDebug() <<  m_notify;
    emit urlNetworkChanged();
}

void Connect::time_now()
{
    qDebug() << "time";
}




