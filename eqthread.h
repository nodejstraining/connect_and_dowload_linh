#ifndef EQTHREAD_H
#define EQTHREAD_H

#include <QThread>
#include "downloadfile.h"
#include <QUrl>

class Eqthread : public QThread
{
    Q_OBJECT

public:
    explicit Eqthread(QObject *parent = 0);
    QUrl returnurl();
    void Seturl(QUrl url);

signals:

public slots:

protected:
    DownloadFile *Dowload;
    void run();
    QUrl urldownload;
    bool Stop;

};

#endif // EQTHREAD_H
