
#ifndef DOWNLOADFILE_H
#define DOWNLOADFILE_H

#include <QFile>
#include <QObject>
#include <QTime>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class DownloadFile : public QObject
{
    Q_OBJECT

public:
    DownloadFile(QObject *parent = 0);
    QString saveFileName(const QUrl &url);
    QString Notify();
    QUrl url;
    int time_down_load;
    QString speed_average = "";
    int file_size = 0;

signals:
    void finished();
    void noteTime();

public slots:
    void downloadProgress(qint64, qint64);
    void startNextdownload();
    void downloadFinished();
    void downloadReadyRead();

private:
    QNetworkAccessManager manager;
    QNetworkReply *currentDownload;
    QFile output;
    QTime downloadTime;
    QString notify;

};

#endif // DOWNLOADFILE_H
