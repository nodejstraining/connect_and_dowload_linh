#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QObject>
#include <QtQml>
#include "connect.h"
#include "downloadfile.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<Connect>("io.qt.examples.connect", 1, 0, "Connect");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

  /*  QString arguments = "http://www.24h.com.vn/the-thao/bao-chi-the-gioi-can-loi-khen-federer-fan-goi-la-nguoi-ngoai-hanh-tinh-c101a889214.html";
    QString arguments1 = "http://www.24h.com.vn/phi-thuong-ky-quac/chuot-khong-lo-chuyen-cham-soc-meo-o-my-c159a890544.html";
    QString arguments2 = "http://doc.qt.io/qt-5/json.html";
    QString arguments3 = "http://doc.qt.io/qt-5/qstringlist.html";
 //   DownloadFile manager;

     Connect abc;
     abc.ReplyfromCtoQml(arguments);
     abc.ReplyfromCtoQml(arguments1);
     abc.ReplyfromCtoQml(arguments2);
     abc.ReplyfromCtoQml(arguments3); */


  //  manager.append(arguments);

    return app.exec();
}
